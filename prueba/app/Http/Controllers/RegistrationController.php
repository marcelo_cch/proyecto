<?php

namespace App\Http\Controllers;

use App\Models\Registration;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registrations = Registration::orderByDesc('id')->get();
        return view('index', compact('registrations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registration');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $datos = $request->validate(
[

    'name' => 'required|max:255',
    'lastnames' => 'required|max:255',
    'type_doc' => 'required|numeric|min:0|max:1',
    'doc_number' => 'required|numeric|digits_between:8,12',
    'email' => 'required|unique:registrations|max:255',


    

]);

    $registration = Registration::create($datos);
    return redirect()->route('prueba.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function show(Registration $registration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function edit(Registration $registration)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Registration $registration)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Registration  $registration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Registration $registration)
    {
        //
    }



    // public function registrations(Request $request)
    // {
    //     $registrations = Registration::all();
    //     return response()->json($registrations);
    // }


    public function registrations()
    {
        // $registrations = Registration::findOrFail('id');
        // return response()->json($registrations);
        return Registration::all();
    }


    public function registrationsparams($id)
    {
        // $registrations = Registration::findOrFail('id');
        // return response()->json($registrations);
        return Registration::find($id);
    }


  
  




}
